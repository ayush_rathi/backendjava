package com.test.springboot.jwt.integration.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.model.User;


@Component
public class UserService   {

	@Autowired
	UserDaoImpl userDaoImpl;
	
	public String findOne()
	{
		User user = userDaoImpl.getByKey(1l);
		System.out.println( user.getFirstName());
		return user.getFirstName();
	}
}
