package com.test.springboot.jwt.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.springboot.jwt.integration.service.impl.UserService;

/*import com.test.model.RandomCity;
import com.test.model.User;*/

/**
 * Created by nydiarra on 06/05/17.
 */
@RestController
@RequestMapping("/springjwt")
public class ResourceController {
   /* @Autowired
    private GenericService userService;*/

    /*@RequestMapping(value ="/cities")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public List<RandomCity> getUser(){
       // return userService.findAllRandomCities();
    	return null;
    }
    
    

    @RequestMapping(value ="/users", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public List<User> getUsers(){
       // return userService.findAllUsers();
    	return null;
    }*/
	@Autowired
	UserService userService;
	@RequestMapping(value ="/cities")
	    public String getUser(){
	       return userService.findOne();
	  //  return "Ayush";
	    }
}
